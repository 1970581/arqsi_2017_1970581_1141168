var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var ReceitaSchema   = new Schema({

    idutente: { type: Number , required: true },
    idmedico: { type: Number , required: true },
    data: { type: Date , required: true }    
    
    
});

module.exports = mongoose.model('Receita', ReceitaSchema);