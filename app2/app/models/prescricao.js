var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var PrescricaoSchema   = new Schema({

    idapresentacao: { type: Number , required: true },
    posologia:      { type: String , required: true },
    idmedicamento:  { type: Number , required: true }
    
});

module.exports = mongoose.model('Prescricao', PrescricaoSchema);