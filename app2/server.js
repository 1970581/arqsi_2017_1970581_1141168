// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;        // set our port

// connect to our database
var mongoose   = require('mongoose');
//mongoose.connect('mongodb://node:node@novus.modulusmongo.net:27017/Iganiq8o');
mongoose.connect('mongodb://default:default@ds127375.mlab.com:27375/arqsidb2');

var Receita     = require('./app/models/receita');
var Prescricao     = require('./app/models/prescricao');

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router


// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});


// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });   
});

// more routes for our API will happen here

// on routes that end in /bears
// ----------------------------------------------------
router.route('/receita')

    // create a bear (accessed at POST http://localhost:8080/api/bears)
    .post(function(req, res) {

        var receita = new Receita();      // create a new instance of the Bear model
        receita.idutente = req.body.idutente;  // set the bears name (comes from the request)
        receita.idmedico = req.body.idmedico;  // set the bears name (comes from the request)
        receita.data = req.body.data;  // set the bears name (comes from the request)
/*
        Receita.validate(receita, function(err){
            if(err) res.json({ message: 'receita nao valida' });
        });
*/
        //Validacao direta:
        if(receita.idutente == undefined ||
            receita.idmedico == undefined ||
            receita.data == undefined
        ) res.json({ message: 'receita validacao falhou!' });
        else 
        // save the bear and check for errors
        receita.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'receita created!' });
        });
        
        
    })

    // get all the bears (accessed at GET http://localhost:8080/api/bears)
    .get(function(req, res) {
        Receita.find(function(err, receitas) {
            if (err)
                res.send(err);

            res.json(receitas);
        });
    });



// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);

