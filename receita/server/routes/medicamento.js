
var express = require('express');
var router = express.Router();
var jwt    = require('jsonwebtoken');
var configuracao = require('../configuracao');
var autenticar = require('../autenticar');


// MIDDLEWARE
router.all('*', function (req, res, next) {    
    
    console.log("Path: %s",req.path);                   //DEBUG: imprimir o path.
    
    var username = autenticar.getNameFromToken(req);    //Obter o username do token.
    console.log("User: %s",username);                   //DEBUG: imprimir o username

    //if(username) req.tokenUserName = username;          // Se existir username colocamos no request.

    autenticar.setPessoaMedico(req,res,next, username);
    

    //next(); // pass control to the next handler
  });

/* GET teste do google */
router.get('/google', function(req, res, next) {
    
    res.redirect('http://google.com');
});


router.get('/validatetoken', function(req, res, next) {
    //res.json({name: 'ola'});
    
    if(req.tokenUserName != undefined){
        var newToken = req.headers['authorization'];
        if(newToken) req.headers['authorization'] = newToken;
    } 
    res.redirect('http://localhost:3000/pessoa/validatetoken');
});




//Obter lista farmacos por id
router.get('/farmacos/:id',function(req, res, next){
    
        var f = require('../servicos/redirecionar');
        var caminho = '/farmacos/' + req.params.id;
        f.tunelget(req,res,next,caminho);
});

//Obter lista farmacos por nome ou GERAL
router.get('/farmacos/:nome?',function(req, res, next){
    
        var f = require('../servicos/redirecionar');
        var caminho = '/farmacos';
        if(req.query.nome){
            var caminho = '/farmacos/?nome=' + req.query.nome;
        }
        
        f.tunelget(req,res,next,caminho);
});

//Obter lista medicamentos com farmaco de id
router.get('/farmacos/:id/medicamentos',function(req, res, next){
    
        var f = require('../servicos/redirecionar');
        var caminho = '/farmacos/' + req.params.id + '/medicamentos';
        f.tunelget(req,res,next,caminho);
});

//Obter lista apresentacoes com farmaco de id
router.get('/farmacos/:id/apresentacoes',function(req, res, next){
    
        var f = require('../servicos/redirecionar');
        var caminho = '/farmacos/' + req.params.id + '/apresentacoes';
        f.tunelget(req,res,next,caminho);
});

//Obter lista posologias com farmaco de id
router.get('/farmacos/:id/posologias',function(req, res, next){
    
        var f = require('../servicos/redirecionar');
        var caminho = '/farmacos/' + req.params.id + '/posologias';
        f.tunelget(req,res,next,caminho);
});

//Obter lista apresentacao de id
router.get('/apresentacoes/:id',function(req, res, next){
    
        var f = require('../servicos/redirecionar');
        var caminho = '/apresentacoes/' + req.params.id;
        f.tunelget(req,res,next,caminho);
});

//Obter lista apresentacoes GERAL
router.get('/apresentacoes',function(req, res, next){
    
        var f = require('../servicos/redirecionar');
        var caminho = '/apresentacoes';
                
        f.tunelget(req,res,next,caminho);
});

//Obter lista possologias GERAL
router.get('/posologias',function(req, res, next){
    
        var f = require('../servicos/redirecionar');
        var caminho = '/posologias';
                
        f.tunelget(req,res,next,caminho);
});

//Obter lista possologias de id
router.get('/posologias/:id',function(req, res, next){
    
        var f = require('../servicos/redirecionar');
        var caminho = '/posologias/' + req.params.id;
        f.tunelget(req,res,next,caminho);
});


router.get('/medicamentos/:id',function(req, res, next){
    
        var f = require('../servicos/redirecionar');
        var caminho = '/medicamentos/' + req.params.id;
        f.tunelget(req,res,next,caminho);
});


// Medicamentos geral, ou por nome.
router.get('/medicamentos/:nome?',function(req, res, next){
    
        var f = require('../servicos/redirecionar');
        var caminho = '/medicamentos';
        if(req.query.nome){
            var caminho = '/medicamentos/?nome=' + req.query.nome;
        }
        
        f.tunelget(req,res,next,caminho);
});




router.get('/medicamentos/:id/apresentacoes',function(req, res, next){
    
        var f = require('../servicos/redirecionar');
        var caminho = '/medicamentos/' + req.params.id + '/apresentacoes';
        f.tunelget(req,res,next,caminho);
});


router.get('/medicamentos/:id/posologias',function(req, res, next){
    
        var f = require('../servicos/redirecionar');
        var caminho = '/medicamentos/' + req.params.id + '/posologias';
        f.tunelget(req,res,next,caminho);
});



module.exports = router;