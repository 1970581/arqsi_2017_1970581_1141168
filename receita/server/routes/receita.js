var express = require('express');
var router = express.Router();
var Receita     = require('../model/receita');
var Pessoa      = require('../model/pessoa');
var configuracao = require('../configuracao');
var autenticar = require('../autenticar');
var mongoose   = require('mongoose');

// MIDDLEWARE
router.all('*', function (req, res, next) {    
  
  console.log("Path: %s",req.path);                   //DEBUG: imprimir o path.
  
  var username = autenticar.getNameFromToken(req);    //Obter o username do token.
  console.log("User: %s",username);                   //DEBUG: imprimir o username

  //if(username) req.tokenUserName = username;          // Se existir username colocamos no request.

  autenticar.setPessoaGeral(req,res,next, username);
  

  //next(); // pass control to the next handler
});


    
//Obtem uma lista de receitas baseado na pessoa
router.get('/',function(req, res, next){
    
    //Se nao for medico mostramos todas as receitas com o utente da pessoa.
    if(!req.pessoa.medico){   
      Receita.find({ idUtente: req.pessoa.id},
        function (err, receitas){
          if(err){
            res.statusCode = 501; //Unautorizated.
            res.json({ success: false, message: err.message });
          }
          else{
            var dto = require('../dto/dto');
            var lista = true;
            dto.receitaDTOList(req, res, next, receitas, dto, lista);
            //res.json(receitas);

          }

        }
      );

    }
    else{  //MEDICO pesquisamos o utente e o campo do medico.
      Receita.find(
        {$or:[{idUtente: req.pessoa.id},{idMedico:req.pessoa.id}]}, 
        function (err, receitas){
          if(err){
            res.statusCode = 501; //Unautorizated.
            res.json({ success: false, message: err.message });
          }
          else{
            var dto = require('../dto/dto');
            var lista = true;
            dto.receitaDTOList(req, res, next, receitas, dto, lista);
            //res.json(receitas);

          }

        }
      );
      
    }
});


// CRIAR UMA RECEITA APENAS POR UM MEDICO.
router.post('/',function(req, res, next){
    if(!req.pessoa.medico){
      res.statusCode = 401; //Unautorizated.
      res.json({ success: false, message: 'Nao es medico.' });
    }
    else{
      var ctrl = require('../controller/criarreceita');
      var receita = new Receita();
      ctrl.adicionarMedico(req,res,next, receita, ctrl);

    }

      
});



//Encontra uma receita por ID.
router.get('/:receita_id',function(req, res, next){
  var ctrl = require('../controller/criarreceita');
  var idDaReceita = req.params.receita_id;


  ctrl.obterReceita(req,res,next, idDaReceita, 
    function (){
      var dto = require('../dto/dto');
      dto.receitaDTOSingle(req, res, next, req.receitaEncontrada, dto);
    }
    );
  

  

});

//Editar uma receita. Apaga tudo e tem de ser construida de raiz.
router.put('/:receita_id',function(req, res, next){
  var ctrl = require('../controller/criarreceita');
  var idDaReceita = req.params.receita_id;
  req.edit = true;

  ctrl.obterReceita(req,res,next, idDaReceita, 
    function (){
      if(!req.receitaEncontrada.hasAviamentos && req.receitaEncontrada.idMedico.equals(req.pessoa._id)){
        var novaReceita = new Receita();
        novaReceita._id = req.receitaEncontrada._id;
        var ctrl = require('../controller/criarreceita');
        
        ctrl.adicionarMedico(req,res,next, novaReceita, ctrl);
      }
      else{        
        res.statusCode = 401; //Unautorizated.
        res.json({ success: false, message: 'Nao e possivel editares.' });
      }
    }
    );

});


router.get('/:receita_id/prescricao/:prescricao_id',function(req, res, next){
  var ctrl = require('../controller/criarreceita');
  var idDaReceita = req.params.receita_id; 


  ctrl.obterReceita(req,res,next, idDaReceita, 
    function (){
      //req.receitaEncontrada.
      var idDaPrescricao = req.params.prescricao_id;      
      var prescricaoEncontrada;
      try{
        for(var i = 0; i < req.receitaEncontrada.prescricoes.length; i++){  //Correccao do index flutoante.
          if(req.receitaEncontrada.prescricoes[i].idPrescricao == idDaPrescricao){
            prescricaoEncontrada = req.receitaEncontrada.prescricoes[i];        
          }

        }
        //prescricaoEncontrada = req.receitaEncontrada.prescricoes[idDaPrescricao];        
      }
      catch(err){
        console.log(err.message);
      }

      if(prescricaoEncontrada){
        //res.json(prescricaoEncontrada);
        var dto = require('../dto/dto');
        dto.prescricaoDTOSingle(req,res,next, prescricaoEncontrada, ctrl);

      }
      else{
        res.statusCode = 404; //Unautorizated.
        res.json({ success: false, message: 'ID prescricao invalida' });
      }
      
    }
    );

});

router.put('/:receita_id/prescricao/:prescricao_id/aviar',function(req, res, next){
  var ctrl = require('../controller/criarreceita');
  var idDaReceita = req.params.receita_id; 

  if(req.pessoa.farmaceutico){
    ctrl.obterReceita(req,res,next, idDaReceita, 
      function (){
        //req.receitaEncontrada.
        var idDaPrescricao = req.params.prescricao_id;      
        var prescricaoEncontrada;
        try{
          for(var i = 0; i < req.receitaEncontrada.prescricoes.length; i++){  //Correccao do index flutoante.
            if(req.receitaEncontrada.prescricoes[i].idPrescricao == idDaPrescricao){
              prescricaoEncontrada = req.receitaEncontrada.prescricoes[i];        
              idDaPrescricao = i;
            }
          }
        }
        catch(err){
          console.log(err.message);
        }

        if(prescricaoEncontrada){
          //res.json(prescricaoEncontrada);
          var aviar = require('../controller/aviar');
          aviar.aviarPescricao(req,res,next, req.receitaEncontrada, idDaPrescricao, aviar);
        }
        else{
          res.statusCode = 404; //Unautorizated.
          res.json({ success: false, message: 'ID prescricao invalida' });
        }
      
      }
    );
  }
  else{
    res.statusCode = 403; //Forbiden.
    res.json({ success: false, message: 'Farmaceutico required.' });
  }



});

module.exports = router;
