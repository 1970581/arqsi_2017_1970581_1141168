var express = require('express');
var router = express.Router();
var jwt    = require('jsonwebtoken');

// connect to our database
var mongoose   = require('mongoose');
var configuracao = require('../configuracao');
mongoose.connect(configuracao.dbConnectionString, {useMongoClient: true});
var Pessoa     = require('../model/pessoa');
var autenticar = require('../autenticar');




// MIDDLEWARE
router.all('*', function (req, res, next) {    
    console.log("Path: %s",req.path);
    
    var username = autenticar.getNameFromToken(req);
    console.log("Username: %s",username);


    next(); // pass control to the next handler
  });






/* GET users listing. */
router.get('/', function(req, res, next) {
    
    Pessoa.find(function(err, pessoas) {
        if (err){
            res.statusCode = 204;
            res.send(err);
        }
        else res.json(pessoas);
    });
  
});


//CRIAR NOVA PESSOA
router.post('/',function(req, res, next) {
    
            var pessoa = new Pessoa();      // create a new instance of the Bear model
            pessoa.nome =           req.body.nome;
            pessoa.email =           req.body.email;
            pessoa.password =       req.body.password;
            pessoa.medico =         req.body.medico;
            pessoa.farmaceutico =   req.body.farmaceutico;

            pessoa.save(function(err) {
                if (err)
                {
                    res.statusCode = 400;
                    //res.send(err);
                    //res.body = err;
                    res.send(err);
                }
                //else res.json({ message: 'Pessoa created!' });
                else {
                    res.statusCode = 201;
                    res.json(pessoa);
                }
            });
});



//DELETE de uma pessoa.
router.delete('/:pessoa_id',function(req, res,next) {
    Pessoa.remove({
        _id: req.params.pessoa_id
    }, function(err, pessoa) {
        if (err){
            res.statusCode = 400;
            res.send(err);
        }
        //else res.json(pessoa);
        else res.json({ message: 'Successfully deleted' });
    });
});

// Token get
router.post('/gettoken',function(req, res, next) {

            if(req.body.nome == undefined || req.body.password == undefined){
                res.statusCode = 400;
                res.json({ success: false, message: 'Nome & Password Necessarios' });

            }
            
            Pessoa.findOne({ 'nome': req.body.nome, 'password': req.body.password }, function (err, pessoa) {
                if(err){
                    res.statusCode = 500;
                    res.json({ success: false, message: 'Erro a tentar recuperar pessoa com esse nome e password.' });
                }
                else{
                    if(pessoa){
                        res.statusCode = 200;

                        var payload = { nome: pessoa.nome };

                        var tokenValue = jwt.sign(
                            payload,
                            configuracao.tokenHash,
                            { expiresIn: 86400 }  // expires in 24 hours
                        );

                        res.json({
                            success: true,
                            message: 'Enjoy your token!',
                            token: tokenValue,
                            medico: pessoa.medico,
                            farmaceutico: pessoa.farmaceutico
                        });
                    }
                    else{
                        res.statusCode = 400;
                        res.json({ success: false, message: 'Name or Password errados.' });

                    }    
                }               
            });
});

// VALIDAR SE O TOKEM E ACEITAVEL
router.get('/validatetoken', function(req, res, next) {

    var tokenValue = req.body.token || req.query.token || req.headers['x-access-token'] || req.headers['authorization'];
    
    if(tokenValue == undefined){
        res.statusCode = 400;
        res.json({ success: false, message: 'Tem de enviar o token' });
    }
    else{
        jwt.verify(tokenValue, configuracao.tokenHash, function(err, decoded) {      
        if (err) {
            res.statusCode = 401; //Unautorizated.
            res.json({ success: false, message: 'Failed to authenticate token.' });    

            //return res.json({ success: false, message: 'Failed to authenticate token.' });    
        } 
        else {
            res.statusCode = 202; //Accepted.
            res.json({ success: true, message: 'Authenticated token.' });      
            // if everything is good, save to request for use in other routes
            //req.decoded = decoded;    
            //next();

          }
  
     });
    }
});


module.exports = router;