var express = require('express');
var router = express.Router();
var Receita     = require('../model/receita');
var Pessoa      = require('../model/pessoa');
var configuracao = require('../configuracao');
var autenticar = require('../autenticar');
var mongoose   = require('mongoose');

// MIDDLEWARE
router.all('*', function (req, res, next) {    
  
  console.log("Path: %s",req.path);                   //DEBUG: imprimir o path.
  
  var username = autenticar.getNameFromToken(req);    //Obter o username do token.
  console.log("User: %s",username);                   //DEBUG: imprimir o username

  //if(username) req.tokenUserName = username;          // Se existir username colocamos no request.

  autenticar.setPessoaGeral(req,res,next, username);
  

  //next(); // pass control to the next handler
});

// Listar prescricoes por utente por data.
router.get('/:nome_utente/prescricao/poraviar/:data?',function(req, res, next){
    var nomeUtente = req.params.nome_utente
    var dataMinima = req.query.data;
    var objData = new Date();
    
    if(dataMinima){
        try{
            objData = new Date(dataMinima);
        }
        catch(err){
            dataMinima = undefined;
            objData = new Date();
        }
    }
    if(isNaN(objData.getTime())) {
        res.statusCode = 400; //Unautorizated.
        res.json({ success: false, message: 'Data invalida: ' + req.query.data });    
        return;
    }
    req.dataMinima = objData;

    if(req.pessoa.nome == nomeUtente || req.pessoa.medico || req.pessoa.farmaceutico){
        var ctrl   = require('../controller/pesquisarutente');
        
        if(!dataMinima){
            ctrl.pesquisarReceitasPorUtente(req,res,next, nomeUtente, ctrl,  function (){
                ctrl.devolverPrescricoesPorAviarDTO(req,res,next, nomeUtente, req.receitasEncontradas, ctrl);
            });
        }
        else{            
            ctrl.pesquisarReceitasPorUtente(req,res,next, nomeUtente, ctrl,  function (){
                ctrl.selecionarReceitasComDataSuperiorA(req,res,next, nomeUtente, req.receitasEncontradas, req.dataMinima, ctrl);
            });

        }
    }
    else{
        res.statusCode = 404; //Unautorizated.
        res.json({ success: false, message: 'Sem premicoes para o visualisar o utente: ' + req.params.nome_utente });
    }
    
      
});



module.exports = router;