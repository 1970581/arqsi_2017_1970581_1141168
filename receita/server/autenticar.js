
var jwt    = require('jsonwebtoken');
var configuracao = require('./configuracao');


// connect to our database
//var mongoose   = require('mongoose');
//mongoose.connect(configuracao.dbConnectionString, {useMongoClient: true});
var Pessoa     = require('./model/pessoa');

exports.getNameFromToken = function (req) {
    var name;
    if(!req ) return name;

    var tokenValue = req.body.token || req.query.token || req.headers['x-access-token'] || req.headers['authorization'];

    if(!tokenValue ) return name;

    var payload;
     try {
          payload = jwt.verify(tokenValue, configuracao.tokenHash);
          return payload.nome;
        }
        catch(err){            
            return name;
        }  
};

//////////////////////////////////////////////////////////

exports.setPessoaMedico = function (req,res,next,nome){
    console.log("Test");
    Pessoa.findOne( { 'nome': nome}, function (err, pessoaEncontrada) {
       var pessoa 
       if(err) {  // Se query der erro.
          console.log("Erro em autenticar.setPessoaMedico");
          req.pessoa = pessoa;
          res.statusCode = 500;
          res.json("Erro em autenticar.setPessoaGeral query");
       }
       else {
           pessoa = pessoaEncontrada;

           if(pessoa == undefined){     // se pessoa vazio.
                res.statusCode = 401;
                res.json({ success: false, message: 'Failed to authenticate token.' });
           }
           else {                       // temos pessoa.
                req.pessoa = pessoa;                
                if(pessoa.medico){
                    //Ja sabemos que é medico. Portanto:
                    var obtertoken = require('./servicos/obtertokenmedicamento');
                    obtertoken.getNameFromTokenAndPutInHeaderRequest(req,res,next);  // Ele depois prosegue com o pedido para a rota invocando o next()
                    //next();             // Medico, prosegue o pediodo.
                }
                else{                   // NAO MEDICO
                    res.statusCode = 401;
                    res.json({ success: false, message: 'Medico required' });
                }

           }
       }
    });
};

exports.setPessoaGeral = function (req,res,next,nome){
    //console.log("Test");
    Pessoa.findOne( { 'nome': nome}, function (err, pessoaEncontrada) {
       var pessoa 
       if(err) {  // Se query der erro.
          console.log("Erro em autenticar.setPessoaGeral");
          req.pessoa = pessoa;
          res.statusCode = 500;
          res.json("Erro em autenticar.setPessoaGeral query");
       }
       else {
           pessoa = pessoaEncontrada;

           if(pessoa == undefined){     // se pessoa vazio.
                res.statusCode = 401;
                res.json({ success: false, message: 'Failed to authenticate token.' });
           }
           else {                       // temos pessoa.
                req.pessoa = pessoa;                
                if(pessoa.medico){
                    //Ja sabemos que é medico. Portanto buscamos token, para o criar receita.
                    var obtertoken = require('./servicos/obtertokenmedicamento');
                    obtertoken.getNameFromTokenAndPutInHeaderRequest(req,res,next);  // Ele depois prosegue com o pedido para a rota invocando o next()
                    //next();             // Medico, prosegue o pediodo.
                }
                else{                   // NAO MEDICO
                    //nao e medico, mas e um dos outros.
                    next();
                }

           }
       }
    });
};


// Verifica se uma determinada pessoa é medica.
exports.isMedico = function (pessoa){
    if (!pessoa) return false;
    if (!pessoa.medico) return false;
    return pessoa.medico;
};