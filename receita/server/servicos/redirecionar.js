var configuracao = require('../configuracao');

//TUNEL DE HTTP GET
exports.tunelget = function (req,res,next, caminhoparcial) {
    var raiz = configuracao.app1url;
    
    var headerValue = req.headers['authorization'];

    var Client = require('node-rest-client').Client;
    var client = new Client();
    var args = {
        data: configuracao.app1login,
        headers: { "authorization": headerValue },
        requestConfig: {
            timeout: 15000
        },
        responseConfig: {
            timeout: 15000 //response timeout 
        }
    };
    

    var link = client.get(raiz + caminhoparcial, args, function (data, response ) {
        res.statusCode = response.statusCode;
        res.json(data);
    });

    link.on('requestTimeout', function (link) {
        console.log('request has expired');
        link.abort();
        if (!res.headersSent){
            res.statusCode = 500;  //bad request
            res.json({ success: false, message: "requestTimeout redirecionar" });
        }
    });

    link.on('responseTimeout', function (link) {
        console.log('response has expired');
        if (!res.headersSent){
            res.statusCode = 500;  //bad request
            res.json({ success: false, message: "responseTimeout redirecionar" });         
        }
    });

    link.on('error', function (err) {
        console.log('request error', err);
        if (!res.headersSent){
            res.statusCode = 500;  //bad request
            res.json({ success: false, message: err.message });
        }
    });

};