var configuracao = require('../configuracao');

exports.getNameFromTokenAndPutInHeaderRequest = function (req,res,next) {
    var Client = require('node-rest-client').Client;
    var client = new Client();
    var args = {
        data: configuracao.app1login,
        headers: { "Content-Type": "application/json" },
        requestConfig: {
            timeout: 15000
        },
        responseConfig: {
            timeout: 15000 //response timeout 
        }
    };
    var caminho = configuracao.app1url + configuracao.getTokenMedicamento;

    var link = client.post(caminho, args, function (data, response) {
        // parsed response body as js object 
        if(false){ // deviar ser para o 'err' mas a fucnao pelos vistos nao o tem.
            res.statusCode = 500;
            res.json({ success: false, message: 'Falhou a obter token gestao Medicamento.' });
        }
        else{
            if(data.token == undefined){
                res.statusCode = 500;
                res.json({ success: false, message: 'Falhou a obter token gestao Medicamento. data.token undefined' });
            }
            else{
                // Temos token, vamos inserilos como header.
                var insertNewToken = "Bearer " + data.token;
                console.log(insertNewToken);
                
                req.headers['authorization'] = insertNewToken;
                
                res.req = req;
                next();
            }
        }
    });

    link.on('requestTimeout', function (link) {
        console.log('request has expired');
        link.abort();
        if (!res.headersSent){
            res.statusCode = 500;  //bad request
            res.json({ success: false, message: "requestTimeout" });
        }
    });

    link.on('responseTimeout', function (link) {
        console.log('response has expired');
        if (!res.headersSent){
            res.statusCode = 500;  //bad request
            res.json({ success: false, message: "responseTimeout" });         
        }
    });

    link.on('error', function (err) {
        console.log('request error', err);
        if (!res.headersSent){
            res.statusCode = 500;  //bad request
            res.json({ success: false, message: err.message });
        }
    });



};