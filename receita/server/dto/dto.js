
var express = require('express');
var router = express.Router();
var Receita     = require('../model/receita');
var Pessoa      = require('../model/pessoa');
var configuracao = require('../configuracao');
var Client = require('node-rest-client').Client;


// Devolve receitas sob a forma de DTO. receitas tem de ser um ARRAY e 
// lista true/false indica se devovolvemos sob a forma de um ARRAY ou apenas o ARRAY[0]
exports.receitaDTOList = function(req,res,next, receitas, ctrl, lista){

    
    var eArray;
    //aListaEumArray = Array.isArray(listaPrescricoes);  // METODO ASYNCRONO, NAO USAR
    try {  //http://www.shamasis.net/2011/08/infinite-ways-to-detect-array-in-javascript/
        receitas && (receitas.length = -1);
        eArray = false;
    }
    catch (er) {
        eArray= true;
    }
    var nElementos = 0;
    if(eArray) nElementos = receitas.length;

    var respostaDTO = new Array();
    

    for( var index = 0; index < nElementos; index++){
        var receitaDTO = {
            type:               'receitaDTO',
            idReceita:          receitas[index]._id,
            nomeMedico:         receitas[index].nomeMedico,
            nomeUtente:         receitas[index].nomeUtente,            
            emailUtente:        receitas[index].emailUtente,            
            data:               receitas[index].data,            
            /*
            prescricoes: [{
                idPrescricao:    { type: Number , required: true },
                idFarmaco:      { type: Number , required: true },
                nomeFarmaco:        { type: String, required: true },
                posologia:      { type: String, required: true },
                quantidade:     { type: Number , default: 1 },
                idApresentacao: { type: Number , required: true },
                apresentacao:   
                {
                    id:         { type: Number , required: true },
                    concentracao:{ type: String, required: true },
                    forma:      { type: String, required: true },            
                    qtd:        { type: String, required: true },
                },
                aviamentos: [{
                    farmaceutico: {type: String, required: true}, 
                    quantidade:     { type: Number , required: true },
                    data: { type: Date , required: true }
                }]
            }]
            */
            

            
        };
        receitaDTO.prescricoes = new Array();

        var nPrescricoes = receitas[index].prescricoes.length;


        for (var indexP = 0; indexP < nPrescricoes; indexP++){
            var prescricaoDTO = {
                idPrescricao:    receitas[index].prescricoes[indexP].idPrescricao,
                idFarmaco:    receitas[index].prescricoes[indexP].idFarmaco,
                
                nomeFarmaco:    receitas[index].prescricoes[indexP].nomeFarmaco,
                posologia:    receitas[index].prescricoes[indexP].posologia,
                quantidade:    receitas[index].prescricoes[indexP].quantidade,
                aviados:    receitas[index].prescricoes[indexP].aviados,
                
                
                idApresentacao: receitas[index].prescricoes[indexP].idApresentacao,
                apresentacao:   {
                    id:         receitas[index].prescricoes[indexP].apresentacao.id,
                    concentracao: receitas[index].prescricoes[indexP].apresentacao.concentracao,
                    forma:      receitas[index].prescricoes[indexP].apresentacao.forma,   
                    qtd:        receitas[index].prescricoes[indexP].apresentacao.qtd,
                }
            };
            if(receitas[index].prescricoes[indexP].nomeMedicamento) prescricaoDTO.nomeMedicamento = receitas[index].prescricoes[indexP].nomeMedicamento;
            receitaDTO.prescricoes.push(prescricaoDTO);

        }


        respostaDTO.push(receitaDTO);

    }
    if(res.statusCode != 201)    res.statusCode = 200;

    if(lista){
        res.json(respostaDTO);  //Enviamos uma lista.

    }
    else{
        res.json(respostaDTO[0]);
    }


    



};

// Interface para criar um DTO de uma receita
exports.receitaDTOSingle = function(req,res,next, receitaSingle, ctrl){
    var lista = false;
    var arrayR = new Array();
    arrayR[0] = receitaSingle;
    ctrl.receitaDTOList(req,res,next, arrayR,ctrl);


};

// Cria um DTO de uma prescricao
exports.prescricaoDTOSingle = function(req,res,next, prescricaoSingle, ctrl){
    
    var prescricaoDTO = {
        idPrescricao:       prescricaoSingle.idPrescricao,
        idFarmaco:          prescricaoSingle.idFarmaco,
        
        nomeFarmaco:        prescricaoSingle.nomeFarmaco,
        posologia:          prescricaoSingle.posologia,
        quantidade:         prescricaoSingle.quantidade,
        aviados:            prescricaoSingle.aviados,
        
        
        idApresentacao:     prescricaoSingle.idApresentacao,
        apresentacao:   {
            id:             prescricaoSingle.apresentacao.id,
            concentracao:   prescricaoSingle.apresentacao.concentracao,
            forma:          prescricaoSingle.apresentacao.forma,   
            qtd:            prescricaoSingle.apresentacao.qtd,
        }
    };
    if(prescricaoSingle.nomeMedicamento) prescricaoDTO.nomeMedicamento = prescricaoSingle.nomeMedicamento;
    
    prescricaoDTO.aviamentos = new Array();

    var index = 0;
    for(index = 0; index < prescricaoSingle.aviamentos.length; index++){
        var aviamento = {
            nomeFarmaceutico:   prescricaoSingle.aviamentos[index].nomeFarmaceutico,
            quantidade:         prescricaoSingle.aviamentos[index].quantidade,
            data:               prescricaoSingle.aviamentos[index].data
        };
        if(prescricaoSingle.aviamentos[index].nomeMedicamento) aviamento.nomeMedicamento = prescricaoSingle.aviamentos[index].nomeMedicamento;
        prescricaoDTO.aviamentos.push(aviamento);
    }


    if(res.statusCode != 201)    res.statusCode = 200;
    res.json(prescricaoDTO);

};