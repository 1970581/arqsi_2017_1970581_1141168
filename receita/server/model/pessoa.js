
var mongoose     = require('mongoose');


var Schema       = mongoose.Schema;

var PessoaSchema   = new Schema({
    nome:               { type: String, required: true , unique: true },
    email:              { type: String, required: true , unique: true },
    password:           { type: String, required: true },
    medico:             { type: Boolean, default: false },
    farmaceutico:       { type: Boolean, default: false }
});

module.exports = mongoose.model('Pessoa', PessoaSchema);