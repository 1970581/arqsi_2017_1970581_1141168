
var mongoose     = require('mongoose');

var mongoose_validator = require("mongoose-id-validator");

var Schema       = mongoose.Schema;

var ReceitaSchema   = new Schema({
    medico:             {type: mongoose.Schema.Types.ObjectId, ref: 'pessoa'},
    utente:             {type: mongoose.Schema.Types.ObjectId, ref: 'pessoa'},
    data: { type: Date , required: true },    
    prescricoes: [{
        farmaco:        { type: String, required: true },
        apresentacao:   { type: String, required: true },
        posologia:      { type: String, required: true },
        quantidade:     { type: Number , required: true },
        aviamentos: [{
            farmaceutico: {type: String, required: true}, 
            quantidade:     { type: Number , required: true },
            data: { type: Date , required: true }
        }]
    }]
    
});
 ReceitaSchema.plugin(mongoose_validator);

module.exports = mongoose.model('Receita', ReceitaSchema);
