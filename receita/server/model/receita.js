
var mongoose     = require('mongoose');
var Pessoa = require('./pessoa');

var mongoose_validator = require("mongoose-id-validator");

var Schema       = mongoose.Schema;

var ReceitaSchema   = new Schema({
    idMedico:           {type: mongoose.Schema.Types.ObjectId, ref: 'Pessoa'},    
    nomeMedico:         { type: String, required: true },
    idUtente:           {type: mongoose.Schema.Types.ObjectId, ref: 'Pessoa'},
    nomeUtente:         { type: String, required: true },
    emailUtente:        { type: String, required: true },
    data:               { type: Date , required: true },   
    hasAviamentos:      { type: Boolean, default: false}, 
    prescricoes: [{
        idPrescricao:    { type: Number , required: true },
        idFarmaco:      { type: Number , required: true },
        nomeFarmaco:        { type: String, required: true },
        nomeMedicamento:     { type: String, required: false },
        posologia:      { type: String, required: true },
        quantidade:     { type: Number , default: 1 },
        aviados:        { type: Number , default: 0 },
        idApresentacao: { type: Number , required: true },
        apresentacao:   
        {
            id:         { type: Number , required: true },
            concentracao:{ type: String, required: true },
            forma:      { type: String, required: true },            
            qtd:        { type: String, required: true },
        },
        aviamentos: [{
            idFarmaceutico:           {type: mongoose.Schema.Types.ObjectId, ref: 'Pessoa'},
            nomeFarmaceutico: {type: String, required: true}, 
            nomeMedicamento: {type: String, required: false}, 
            quantidade:     { type: Number , required: true },
            data: { type: Date , required: true }
        }]
    }]
    
});
 ReceitaSchema.plugin(mongoose_validator);

module.exports = mongoose.model('Receita', ReceitaSchema);
