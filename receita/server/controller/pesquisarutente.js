var express = require('express');
var router = express.Router();
var Receita     = require('../model/receita');
var Pessoa      = require('../model/pessoa');
var configuracao = require('../configuracao');
var Client = require('node-rest-client').Client;

var mongoose   = require('mongoose');


//Pesquisa as receitas de um utente, e chama callback proximo.
exports.pesquisarReceitasPorUtente = function (req,res,next, nomeUtente, ctrl, proximo){
    
    

    Receita.find( { 'nomeUtente': nomeUtente}, function (err, receitasEncontradas) {
        var pessoa 
        if(err) {  // Se query der erro.
           console.log("Erro em autenticar.setPessoaGeral");
           req.pessoa = pessoa;
           res.statusCode = 500;
           res.json(err);
        }
        else{
            if(!receitasEncontradas){
                res.statusCode = 500;
                res.json({ success: false, message: 'Erro interno de pesquisa das receitas. Receitas vazio.' });
            }
            else{
                req.receitasEncontradas = receitasEncontradas;
                proximo();
            }
        }
    });
};

//Devolve as prescricoes por Aviar sob a forma de um DTO.
exports.devolverPrescricoesPorAviarDTO = function (req,res,next, nomeUtente, receitasEncontradas, ctrl){

    var listaPrescricoesDTO = new Array();
    var sucesso = true;
    try{
        for(var index = 0; index < receitasEncontradas.length; index++){
            var tudoAviado = true;
            for(var i = 0; i < receitasEncontradas[index].prescricoes.length; i++){
                if(receitasEncontradas[index].prescricoes[i].aviados < receitasEncontradas[index].prescricoes[i].quantidade){
                    var prescricaoSingle = receitasEncontradas[index].prescricoes[i];
                    listaPrescricoesDTO.push(
                        {
                            type:           "prescricao por aviar",
                            idReceita:     receitasEncontradas[index]._id,
                            data:          receitasEncontradas[index].data,

                            idPrescricao:       prescricaoSingle.idPrescricao,
                            idFarmaco:          prescricaoSingle.idFarmaco,
                            
                            nomeFarmaco:        prescricaoSingle.nomeFarmaco,
                            posologia:          prescricaoSingle.posologia,
                            quantidade:         prescricaoSingle.quantidade,
                            aviados:            prescricaoSingle.aviados,
                            
                            
                            idApresentacao:     prescricaoSingle.idApresentacao,
                            apresentacao:   {
                                id:             prescricaoSingle.apresentacao.id,
                                concentracao:   prescricaoSingle.apresentacao.concentracao,
                                forma:          prescricaoSingle.apresentacao.forma,   
                                qtd:            prescricaoSingle.apresentacao.qtd,
                            }
                        }
                    );
                }
            }         
        }
    }
    catch(err){
        sucesso = false;
        res.statusCode = 500;
        res.json({ success: false, message: err.message });
    }

    if(sucesso){
        res.statusCode = 200;
        res.json(listaPrescricoesDTO);
    }


};





// escolher apenas as receitas com data que interesam para enviar para DTO.
exports.selecionarReceitasComDataSuperiorA = function (req,res,next, nomeUtente, receitasEncontradas, data, ctrl){
    if (!data) data = Date();
    var dataComparar = data;
    var dataReceita = new Date();
    var receitasEmData = new Array();
    var sucess = true;

    try{
        for(var index = 0; index < receitasEncontradas.length; index++){            

            dataReceita = new Date(receitasEncontradas[index].data );
            
            if (dataReceita >  dataComparar){  //Se receita com data mais cedo que data de prescricao.
                receitasEmData.push(receitasEncontradas[index]);
            }
        }
    }
    catch(err){
        console.log(err.message);
        sucess = false;
    }

    if(sucess){
        ctrl.devolverPrescricoesPorAviarDTO(req,res,next, nomeUtente, receitasEmData, ctrl);
    }
    else{
        ctrl.devolverPrescricoesPorAviarDTO(req,res,next, nomeUtente, receitasEncontradas, ctrl);
    }



};