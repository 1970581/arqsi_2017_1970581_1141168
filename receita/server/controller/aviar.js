var express = require('express');
var router = express.Router();
var Receita     = require('../model/receita');
var Pessoa      = require('../model/pessoa');
var configuracao = require('../configuracao');
var Client = require('node-rest-client').Client;

var mongoose   = require('mongoose');


// Adiciona os campos do medico a receita
exports.aviarPescricao = function (req,res,next, receita, numeroPrescricao, ctrl){
    var possivelNumero = req.body.quantidade;

    var n = 0;
    var eNumero = false;
    try{
        n = parseInt(possivelNumero);
        eNumero = !isNaN(n);
    }
    catch(err){
        console.log(err.message);
    }

    var jaAviadas = receita.prescricoes[numeroPrescricao].aviados;
    var max = receita.prescricoes[numeroPrescricao].quantidade;
    
    // A quantidade a aviar tem de ser um inteiro, positivo e inferior ao que falta aviar.
    if (eNumero && n > 0 && n <= max - jaAviadas){
        ctrl.aviarPescricaoQuantidadeCorrecta(req,res,next, receita, numeroPrescricao, n, ctrl);
    }
    else{
        res.statusCode = 400; //BAD REQUEST
        res.json({ success: false, message: 'Quantidade a aviar para prescricao invalida.' });
    }
};

// 
exports.aviarPescricaoQuantidadeCorrecta = function (req,res,next, receita, numeroPrescricao, quantidadeAviar, ctrl){
    var sucesso;
    var dataHoje = Date();

    try{
        receita.hasAviamentos = true;
        receita.prescricoes[numeroPrescricao].aviados += quantidadeAviar;
        
        receita.prescricoes[numeroPrescricao].aviamentos.push(
            {
                idFarmaceutico:         req.pessoa._id,
                nomeFarmaceutico:       req.pessoa.nome, 
                nomeMedicamento:        req.body.nomeMedicamento,
                quantidade:             quantidadeAviar,
                data:                   dataHoje
                
            }
        );

        sucesso = true;
    }
    catch(err){
        res.statusCode = 500; //BAD REQUEST
        res.json({ success: false, message: err.message });
    }
    
    if(sucesso){
        res.statusCode = 200; 
        ctrl.salvarAviamento(req,res,next, receita, numeroPrescricao, ctrl);
    }
};


exports.salvarAviamento = function (req,res,next, receita, numeroPrescricao, ctrl){

    receita.update( receita,function(err) {
        if (err)
        {
            res.statusCode = 500;
            res.send(err);
        }
        else {
            res.statusCode = 201;
            var dto = require('../dto/dto');
            dto.prescricaoDTOSingle(req,res,next, receita.prescricoes[numeroPrescricao], dto);
        }
    });

};