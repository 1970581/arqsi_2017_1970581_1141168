var express = require('express');
var router = express.Router();
var Receita     = require('../model/receita');
var Pessoa      = require('../model/pessoa');
var configuracao = require('../configuracao');
var Client = require('node-rest-client').Client;

var mongoose   = require('mongoose');
//mongoose.connect(configuracao.dbConnectionString, {useMongoClient: true});



// Adiciona os campos do medico a receita
exports.adicionarMedico = function (req,res,next, receita, ctrl){
    
    receita.idMedico = req.pessoa._id;
    receita.nomeMedico = req.pessoa.nome;
    
    receita.data = Date();
    
    //var ctrl = require('./criarreceita');
    ctrl.adicionarUtente(req,res,next,receita, ctrl);
    
};

//Adiciona os campos do Utente a receita
exports.adicionarUtente = function(req,res,next, receita, ctrl){
    console.log("");
    
    var nome = req.body.utente;

    Pessoa.findOne( { 'nome': nome}, function (err, pessoaEncontrada) {
        var pessoa = pessoaEncontrada;
        if(err) {  // Se query der erro.
           console.log("Erro a encontar utente para criar receita.");
           res.statusCode = 500;
           res.json("Erro em encontar utente para criar receita.");
        }
        else {
            if(pessoa == undefined){     // se pessoa vazio.
                res.statusCode = 400;  //bad request
                res.json({ success: false, message: 'Utente nao encontrado.' });
           }
           else {                       // temos pessoa.
                req.utente = pessoa;                
                receita.idUtente = pessoa._id;
                receita.nomeUtente = pessoa.nome;
                receita.emailUtente = pessoa.email;

                //var ctrl = require('./criarreceita');
                ctrl.processarPrescricoes(req,res,next,receita, ctrl);
           }

        }
    });
};

exports.processarPrescricoes = function(req,res,next, receita, ctrl){

    var listaPrescricoes = req.body.prescricao;
    var eArray;
    //aListaEumArray = Array.isArray(listaPrescricoes);  // METODO ASYNCRONO, NAO USAR
    try {  //http://www.shamasis.net/2011/08/infinite-ways-to-detect-array-in-javascript/
        listaPrescricoes && (listaPrescricoes.length = -1);
        eArray = false;
    }
    catch (er) {
        eArray= true;
    }
    var nElementos = 0;
    if(eArray) nElementos = listaPrescricoes.length;
    
    if(nElementos == 0){        
        res.statusCode = 400;  //Bad request
        res.json({ success: false, message: 'Lista de prescricoes nao encontrada ou tamanho zero.' });
    }
    else{
        //receita.prescricao = new Array();
        ctrl.processarUmaPrescricao(req,res,next,receita, nElementos, ctrl);
    }
};


exports.processarUmaPrescricao = function(req,res,next, receita, nPrescricao, ctrl){
    var n = nPrescricao;
    if(n < 0) n = 0;

    if(n == 0){
        
        ctrl.guardarReceita(req,res,next,receita, ctrl);
    }
    else{
        ctrl.obterApresentacao(req,res,next, receita, n, ctrl);

        //ctrl.processarUmaPrescricao(req,res,next, receita, nNext, ctrl);

    }

};


exports.obterApresentacao = function(req,res,next, receita, n, ctrl){

    var listaPrescricoes = req.body.prescricao;

    var p = listaPrescricoes[n-1];   // 0 -> apresentacao 1.
    var isNumber;

    if(p.idApresentacao == undefined || isNaN(p.idApresentacao)) {
        res.statusCode = 400;  //bad request
        res.json({ success: false, message: 'Id de um apresentacao nao e um numero' });
    }
    else{
        var caminho = configuracao.app1url + configuracao.getApresentacao;
        var headerValue = req.headers['authorization'];
    
        var client = new Client();
        var args = {
            data: configuracao.app1login,
            headers: { "authorization": headerValue },
            requestConfig: {
                timeout: 15000
            },
            responseConfig: {
                timeout: 15000 //response timeout 
            }
        };
    
        
        var link = client.get(caminho + '/' + p.idApresentacao, args, function (data, response ) {
            if(response == undefined || response.statusCode != 200){
                res.statusCode = 400;  //bad request
                res.json({ success: false, message: 'Verifica o teu ID de apresentacao', data: data });
            }
            else{
                //RESPOSTA POSITIVA, VAMOS COPIAR.
                try{
                    var nomeMedicamentoRecebido;
                    if(p.nomeMedicamento) nomeMedicamentoRecebido = p.nomeMedicamento;


                    receita.prescricoes.push({
                        idPrescricao:   n-1,
                        idFarmaco:      data.idFarmaco,
                        nomeFarmaco:    data.nomeFarmaco,
                        nomeMedicamento: nomeMedicamentoRecebido,
                        idApresentacao: data.id,
                        apresentacao: {
                            id:             data.id,
                            concentracao:   data.concentracao,
                            forma:          data.forma,
                            qtd:            data.qtd
                        },
                        posologia: req.body.prescricao[n-1].posologia,
                        quantidade: req.body.prescricao[n-1].quantidade


                    }
                    );
                    //receita.prescricoes[receita.prescricoes.length-1].aviamentos = new Array();

                }
                catch(err){
                    console.log(err.message);
                }
                var nNext = n -1;
                ctrl.processarUmaPrescricao(req,res,next,receita, nNext, ctrl);
                //res.json(receita);
                //ctrl.guardarReceita(req,res,next,receita, ctrl);
            }
        });
        
        link.on('requestTimeout', function (link) {
            console.log('request has expired');
            link.abort();
            if (!res.headersSent){
                res.statusCode = 500;  //bad request
                res.json({ success: false, message: "request Timeout on APP2 get apresentacao" });
            }
        });

        link.on('responseTimeout', function (link) {
            console.log('response has expired');
            if (!res.headersSent){
                res.statusCode = 500;  //bad request
                res.json({ success: false, message: "responseTimeout on APP2 get apresentacao" });         
            }
        });

        link.on('error', function (err) {
            console.log('request error', err);
            if (!res.headersSent){
                res.statusCode = 500;  //bad request
                res.json({ success: false, message: err.message });
            }
        });



    }

}




exports.guardarReceita = function(req,res,next, receita, ctrl){
    if(req.edit){
        //EDITAR RECEITA.
        receita.update( receita,function(err) {
            if (err)
            {
                res.statusCode = 400;
                res.send(err);
            }
            else {
                res.statusCode = 201;
                var dto = require('../dto/dto');
                dto.receitaDTOSingle(req, res, next, receita, dto);
            }
        });

    }
    else{    // NAO E EDITAR MAS CRIAR.
        receita.save(function(err) {
            if (err)
            {
                res.statusCode = 400;
                res.send(err);
            }
            else {
                res.statusCode = 201;
                var dto = require('../dto/dto');
                dto.receitaDTOSingle(req, res, next, receita, dto);
                ctrl.enviarEmailFireAndForget(req,res,next, receita.emailUtente, receita._id);
            }
        });
    }
};

exports.obterReceita = function(req,res,next, idReceita, proximo){
    var userId;
    try{
      userId = mongoose.Types.ObjectId(idReceita);
    }
    catch(err2){
      console.log(err2.message);
    }
  
    
    Receita.findById( userId, function (err, receitaEncontrada) {
      if(err) {  // Se query der erro.
        res.statusCode = 501; //Unautorizated.
        res.json({ success: false, message: err.message });
     }
     else {
       if(receitaEncontrada){
        var dto = require('../dto/dto');
        var lista = false;
        //var arrayReceitas = new Array();
        //arrayReceitas.join(receitaEncontrada);
        //dto.receitaDTOList(req, res, next, arrayReceitas, dto, lista);
        var idPessoa = req.pessoa._id;
        if( req.pessoa.farmaceutico || receitaEncontrada.idMedico.equals(idPessoa) || receitaEncontrada.idUtente.equals(idPessoa)){
            req.receitaEncontrada = receitaEncontrada;
            proximo();
          //dto.receitaDTOSingle(req, res, next, receitaEncontrada, dto);
        }else{
            res.statusCode = 404; //Unautorizated.
            res.json({ success: false, message: "Id sem premicoes" });
        }
        
       }
       else{  //nao encontrada receita.
        res.statusCode = 404; //Unautorizated.
        res.json({ success: false, message: "Id receita errado" });
  
       }     
      
     }
  
    });
};


exports.enviarEmailFireAndForget = function(req,res,next, email, idReceita){
    var nodemailer = require('nodemailer'); 

    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: configuracao.mailSenderMail,
          pass: configuracao.mailSenderPass
        }
      });
      
      var mailOptions = {
        from: configuracao.mailSenderMail,
        to: email,
        subject: 'Receita criada com id: ' + idReceita,
        text: 'Receita criada com id: ' + idReceita + ' . Visite o nosso site.'
      };
      
      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      }); 

};