﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using arqsi_2017_1970581_1141168.Models;
using arqsi_2017_1970581_1141168.DTO;
using Microsoft.AspNetCore.Authorization;
using arqsi_2017_1970581_1141168.Servicos;

namespace arqsi_2017_1970581_1141168.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/MedicamentoApresentacoes")]
    public class MedicamentoApresentacoesController : Controller
    {
        private readonly arqsi_2017_1970581_1141168Context _context;

        public MedicamentoApresentacoesController(arqsi_2017_1970581_1141168Context context)
        {
            _context = context;
        }

        // GET: api/MedicamentoApresentacoes
        [HttpGet]
        public IEnumerable<MedicamentoApresentacao> GetMedicamentoApresentacao()
        {
            return _context.MedicamentoApresentacao;
        }

        // GET: api/MedicamentoApresentacoes/5
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetMedicamentoApresentacao([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medicamentoApresentacao = await _context.MedicamentoApresentacao.SingleOrDefaultAsync(m => m.Id == id);

            if (medicamentoApresentacao == null)
            {
                return NotFound();
            }

            return Ok(medicamentoApresentacao);
        }

        // PUT: api/MedicamentoApresentacoes/5
        [HttpPut("{id:int}")]
        public async Task<IActionResult> PutMedicamentoApresentacao([FromRoute] long id, [FromBody] MedicamentoApresentacaoDTO medicamentoApresentacaoDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != medicamentoApresentacaoDTO.Id)
            {
                return BadRequest();
            }


            var queryM = from m in _context.Medicamento
                         where m.Id == medicamentoApresentacaoDTO.idMedicamento
                         select m;
            Medicamento medicamento = await queryM.SingleOrDefaultAsync();
            if (medicamento == null) return BadRequest(medicamentoApresentacaoDTO); // Confirmamos que exite medicamento com aquele id


            var queryA = from a in _context.Apresentacao
                         where a.Id == medicamentoApresentacaoDTO.idApresentacao
                         select a;
            Apresentacao apresentacao = await queryA.SingleOrDefaultAsync();
            if (apresentacao == null) return BadRequest(medicamentoApresentacaoDTO); // Confirmamos que exite apresentacao com aquele id

            MedicamentoApresentacao medicamentoApresentacaoAlterado = new MedicamentoApresentacao()
            {
                apresentacao = apresentacao,
                medicamento = medicamento
            };

            if (Unicos.IsUnico(_context, medicamentoApresentacaoAlterado) == false) return BadRequest("Duplicado");


            //Obter o original
            var queryMA = from ma in _context.MedicamentoApresentacao where ma.Id == medicamentoApresentacaoDTO.Id select ma;
            MedicamentoApresentacao medicamentoApresentacaoOriginal = await queryMA.SingleOrDefaultAsync();
            if (medicamentoApresentacaoOriginal == null) return BadRequest(medicamentoApresentacaoDTO); // Confirmamos que exite apresentacao com aquele id

            //alterar
            medicamentoApresentacaoOriginal.apresentacao = apresentacao;
            medicamentoApresentacaoOriginal.medicamento = medicamento;


            _context.Entry(medicamentoApresentacaoOriginal).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedicamentoApresentacaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MedicamentoApresentacoes
        [HttpPost]
        public async Task<IActionResult> PostMedicamentoApresentacao([FromBody] MedicamentoApresentacaoDTO medicamentoApresentacaoDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var queryM = from m in _context.Medicamento
                         where m.Id == medicamentoApresentacaoDTO.idMedicamento
                         select m;
            Medicamento medicamento = await queryM.SingleOrDefaultAsync();
            if (medicamento == null) return BadRequest(medicamentoApresentacaoDTO); // Confirmamos que exite medicamento com aquele id


            var queryA = from a in _context.Apresentacao
                        where a.Id == medicamentoApresentacaoDTO.idApresentacao
                        select a;
            Apresentacao apresentacao = await queryA.SingleOrDefaultAsync();
            if (apresentacao == null) return BadRequest(medicamentoApresentacaoDTO); // Confirmamos que exite apresentacao com aquele id

            MedicamentoApresentacao medicamentoApresentacao = new MedicamentoApresentacao() {
                apresentacao = apresentacao,
                medicamento = medicamento
            };

            if (Unicos.IsUnico(_context, medicamentoApresentacao) == false) return BadRequest("Duplicado");

            _context.MedicamentoApresentacao.Add(medicamentoApresentacao);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMedicamentoApresentacao", new { id = medicamentoApresentacao.Id }, medicamentoApresentacao);
        }

        // DELETE: api/MedicamentoApresentacoes/5
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> DeleteMedicamentoApresentacao([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medicamentoApresentacao = await _context.MedicamentoApresentacao.SingleOrDefaultAsync(m => m.Id == id);
            if (medicamentoApresentacao == null)
            {
                return NotFound();
            }

            _context.MedicamentoApresentacao.Remove(medicamentoApresentacao);
            await _context.SaveChangesAsync();

            return Ok(medicamentoApresentacao);
        }

        private bool MedicamentoApresentacaoExists(long id)
        {
            return _context.MedicamentoApresentacao.Any(e => e.Id == id);
        }
    }
}