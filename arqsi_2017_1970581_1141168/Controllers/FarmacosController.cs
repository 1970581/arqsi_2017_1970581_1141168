﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using arqsi_2017_1970581_1141168.Models;
using arqsi_2017_1970581_1141168.DTO;
using arqsi_2017_1970581_1141168.Servicos;
using Microsoft.AspNetCore.Authorization;

namespace arqsi_2017_1970581_1141168.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Farmacos")]
    public class FarmacosController : Controller
    {
        private readonly arqsi_2017_1970581_1141168Context _context;

        public FarmacosController(arqsi_2017_1970581_1141168Context context)
        {
            _context = context;
        }

        // GET: api/Farmacos
        [HttpGet]
        public IEnumerable<FarmacoDTO> GetFarmaco( string nome)
        {
            if (nome == null) return DTOFactory.BuildList(_context.Farmaco);  // Se nao existir ?nome="..." retorna todos os farmacos.

            //A existir retorna os com o ?nome=X
            var query = from f in _context.Farmaco where f.nome == nome select f;
            return DTOFactory.BuildList(query);
        }




        // GET: api/Farmacos/5
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetFarmaco([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var farmaco = await _context.Farmaco.SingleOrDefaultAsync(m => m.Id == id);

            if (farmaco == null)
            {
                return NotFound();
            }

            //return Ok(farmaco);   -> DTO
            return Ok(DTOFactory.Build(farmaco));
        }

        // GET: api/Farmacos/5/Apresentacoes - mostra todas as apresentacoes de um farmaco de Id.
        [HttpGet("{id:int}/Apresentacoes")]
        public async Task<IActionResult> GetApresentacoesDeFarmaco([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var farmaco = await _context.Farmaco.SingleOrDefaultAsync(m => m.Id == id);

            if (farmaco == null)
            {
                return NotFound();
            }

            // SQL query:  retorna a lista de apresentacoes (Funciona)
            //var lista = from a in _context.Apresentacao where a.farmaco.Id == farmaco.Id select a;
            //return Ok(DTOFactory.BuildList(lista));

            // SQL query: retorna a lista de Id de apresentacoes. ANTIGO
            //var listaInt = from a in _context.Apresentacao where a.farmaco.Id == farmaco.Id select a.Id;

            var apresentacoes = _context.Apresentacao.Where(a => a.farmaco.Id == id);

            List<ApresentacaoDTO> resposta = new List<ApresentacaoDTO>();
            foreach (Apresentacao ap in apresentacoes)
            {
                resposta.Add(DTOFactory.Build(ap, ap.farmaco.Id, ap.farmaco.nome));
            }

            return Ok(resposta);
        }

        // GET: api/Farmacos/5/Apresentacoes - mostra todas as apresentacoes de um farmaco de Id.
        [HttpGet("{id:int}/Posologias")]
        public async Task<IActionResult> GetPosologiasDeFarmaco([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var farmaco = await _context.Farmaco.SingleOrDefaultAsync(m => m.Id == id);

            if (farmaco == null)
            {
                return NotFound();
            }

            // SQL query:  retorna a lista de apresentacoes (Funciona)
            //var lista = from a in _context.Apresentacao where a.farmaco.Id == farmaco.Id select a;
            //return Ok(DTOFactory.BuildList(lista));

            // SQL query: retorna a lista de Id de posologias que pertencem a um determinado farmaco ligadas pelas apresentacoes.
            // Antiga, so devolia ID's.
            /*
            var query = from a in _context.Apresentacao join pos in _context.Posologia
                        on a.Id equals pos.apresentacao.Id
                        where a.farmaco.Id == farmaco.Id select pos.Id;
            */

            var posologias = from a in _context.Apresentacao
                                         join pos in _context.Posologia.Include(i => i.apresentacao)
                                         on a.Id equals pos.apresentacao.Id
                                         where a.farmaco.Id == farmaco.Id
                                         select pos;

            List<PosologiaDTO> resposta = new List<PosologiaDTO>();
            foreach (Posologia p in posologias)
            {
                resposta.Add(DTOFactory.Build(p, p.apresentacao.Id));
            }


            return Ok(resposta);
            //return Ok(query);
        }


        // PUT: api/Farmacos/5
        [HttpPut("{id:int}")]
        public async Task<IActionResult> PutFarmaco([FromRoute] long id, [FromBody] Farmaco farmaco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != farmaco.Id)
            {
                return BadRequest();
            }

            if (Unicos.IsUnico(_context, farmaco) == false)
            {
                return BadRequest("Duplicado");
            }

            _context.Entry(farmaco).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FarmacoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Farmacos
        [HttpPost]
        public async Task<IActionResult> PostFarmaco([FromBody] Farmaco farmaco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (Unicos.IsUnico(_context, farmaco) == false) {
                return BadRequest("Duplicado");
            }

            _context.Farmaco.Add(farmaco);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFarmaco", new { id = farmaco.Id }, DTOFactory.Build(farmaco));
        }

        // DELETE: api/Farmacos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFarmaco([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var farmaco = await _context.Farmaco.SingleOrDefaultAsync(m => m.Id == id);
            if (farmaco == null)
            {
                return NotFound();
            }

            _context.Farmaco.Remove(farmaco);
            await _context.SaveChangesAsync();

            //return Ok(farmaco);
            return Ok(DTOFactory.Build(farmaco));  // DTO
        }

        private bool FarmacoExists(long id)
        {
            return _context.Farmaco.Any(e => e.Id == id);
        }
    }
}