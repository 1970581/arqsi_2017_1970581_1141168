﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using arqsi_2017_1970581_1141168.Models;
using arqsi_2017_1970581_1141168.DTO;
using arqsi_2017_1970581_1141168.Servicos;
using Microsoft.AspNetCore.Authorization;

namespace arqsi_2017_1970581_1141168.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Apresentacoes")]
    public class ApresentacoesController : Controller
    {
        private readonly arqsi_2017_1970581_1141168Context _context;

        public ApresentacoesController(arqsi_2017_1970581_1141168Context context)
        {
            _context = context;
        }

        // GET: api/Apresentacoes
        [HttpGet]
        public IEnumerable<ApresentacaoDTO> GetApresentacao()
        {
            var apresentacoes = _context.Apresentacao.Include(a => a.farmaco).ToList();

            List<ApresentacaoDTO> resposta = new List<ApresentacaoDTO>();
            foreach (Apresentacao ap in apresentacoes) {
                resposta.Add(DTOFactory.Build(ap, ap.farmaco.Id, ap.farmaco.nome));
            }
            return resposta;
            //return _context.Apresentacao;

        }

        // GET: api/Apresentacoes/5
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetApresentacao([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apresentacao = await _context.Apresentacao.Include(a => a.farmaco).SingleOrDefaultAsync(a => a.Id == id);

            if (apresentacao == null)
            {
                return NotFound();
            }
            /*
            var query = from a in _context.Apresentacao
                        where a.Id == id
                        select a.farmaco.Id;

            long IdFarmaco = await query.SingleOrDefaultAsync();
            */
            return Ok(DTOFactory.Build(apresentacao,apresentacao.farmaco.Id,apresentacao.farmaco.nome));
        }

        // PUT: api/Apresentacoes/5
        [HttpPut("{id:int}")]
        public async Task<IActionResult> PutApresentacao([FromRoute] long id, [FromBody] ApresentacaoDTO apresentacaoDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != apresentacaoDTO.Id)
            {
                return BadRequest();
            }
            //obter o farmaco indicado pelo edit
            var queryf = from f in _context.Farmaco where f.Id == apresentacaoDTO.IdFarmaco select f;
            Farmaco farmaco2 = await queryf.FirstOrDefaultAsync();

            if (farmaco2 == null) return BadRequest("farmaco inexistente");

            //int conc;
            //bool teste = Int32.TryParse(apresentacaoDTO.concentracao, out conc);
            //if (teste == false) return BadRequest("erro no int da concentracao");

            Apresentacao novaApresentacao = new Apresentacao()
            {
                farmaco = farmaco2,
                forma = apresentacaoDTO.forma,
                concentracao = apresentacaoDTO.concentracao,
                qtd = apresentacaoDTO.qtd
            };

            if (novaApresentacao.farmaco == null) Console.WriteLine("erro novaapp nulla");

            if (Unicos.IsUnico(_context, novaApresentacao) == false)
            {
                return BadRequest("Duplicado");
            }

            var query = from ap in _context.Apresentacao where ap.Id == id select ap;
            Apresentacao apresentacao = await query.FirstOrDefaultAsync();

            if (apresentacao == null) return BadRequest("apresentacao indicada null");


            apresentacao.farmaco = farmaco2;
            apresentacao.forma = novaApresentacao.forma;
            apresentacao.concentracao = novaApresentacao.concentracao;
            apresentacao.qtd = novaApresentacao.qtd;

            _context.Entry(apresentacao).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ApresentacaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Apresentacoes
        [HttpPost]
        public async Task<IActionResult> PostApresentacao([FromBody] CreateApresentacaoDTO createApresentacaoDTO)
        {
            if ( !createApresentacaoDTO.Valid() ) return BadRequest(createApresentacaoDTO);  // Verificamos se o DTO de entrada correctamente preenchido.

            // Obtemos o farmaco indicado por ID no DTO
            var query = from f in _context.Farmaco
                          where f.Id == createApresentacaoDTO.IdFarmaco
                          select f;
            Farmaco farmaco = await query.SingleOrDefaultAsync();

            if (farmaco == null) return BadRequest(createApresentacaoDTO); // Confirmamos que exite farmaco com aquele id
            
            // Criamos apresentacao.
            Apresentacao apresentacao = new Apresentacao() {
                farmaco = farmaco,
                forma = createApresentacaoDTO.forma,
                concentracao = createApresentacaoDTO.concentracao,
                qtd = createApresentacaoDTO.qtd
                };

            if (Unicos.IsUnico(_context, apresentacao) == false)
            {
                return BadRequest("Duplicado");
            }


            _context.Apresentacao.Add(apresentacao);
            await _context.SaveChangesAsync();

            ApresentacaoDTO dto = DTOFactory.Build(apresentacao, createApresentacaoDTO.IdFarmaco);

            return CreatedAtAction("GetApresentacao", new { id = apresentacao.Id }, dto);
        }


        // DELETE: api/Apresentacoes/5
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> DeleteApresentacao([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apresentacao = await _context.Apresentacao.SingleOrDefaultAsync(m => m.Id == id);
            if (apresentacao == null)
            {
                return NotFound();
            }

            _context.Apresentacao.Remove(apresentacao);
            await _context.SaveChangesAsync();

            return Ok(DTOFactory.Build(apresentacao));
        }

        private bool ApresentacaoExists(long id)
        {
            return _context.Apresentacao.Any(e => e.Id == id);
        }
    }
}