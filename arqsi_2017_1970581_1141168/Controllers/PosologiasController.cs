﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using arqsi_2017_1970581_1141168.Models;
using arqsi_2017_1970581_1141168.DTO;
using arqsi_2017_1970581_1141168.Servicos;
using Microsoft.AspNetCore.Authorization;

namespace arqsi_2017_1970581_1141168.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Posologias")]
    public class PosologiasController : Controller
    {
        private readonly arqsi_2017_1970581_1141168Context _context;

        public PosologiasController(arqsi_2017_1970581_1141168Context context)
        {
            _context = context;
        }

        // GET: api/Posologias
        [HttpGet]
        public async Task<IEnumerable<PosologiaDTO>> GetPosologiaAsync()
        {
            List<PosologiaDTO> lista = new List<PosologiaDTO>();
            foreach (Posologia posologia in _context.Posologia) {
                 
                var query = from p in _context.Posologia where p.Id == posologia.Id select p.apresentacao.Id;
                long idApresentacao = await query.SingleOrDefaultAsync();
                lista.Add(DTOFactory.Build(posologia,idApresentacao));
            }
            return lista;
        }

        // GET: api/Posologias/5
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetPosologia([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var posologia = await _context.Posologia.SingleOrDefaultAsync(m => m.Id == id);

            if (posologia == null)
            {
                return NotFound();
            }

            var query = from p in _context.Posologia where p.Id == posologia.Id select p.apresentacao.Id;
            long idApresentacao = await query.SingleOrDefaultAsync();

            return Ok(DTOFactory.Build(posologia, idApresentacao));
        }

        // PUT: api/Posologias/5
        [HttpPut("{id:int}")]
        public async Task<IActionResult> PutPosologia([FromRoute] long id, [FromBody] PosologiaDTO posologiaDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != posologiaDTO.Id)
            {
                return BadRequest();
            }

            // Vamos obter a posologia Original
            var queryP = from p in _context.Posologia
                         where p.Id == posologiaDTO.Id
                         select p;
            Posologia posologiaOriginal = await queryP.SingleOrDefaultAsync();

            // Se nao exitir damos erro
            if (posologiaOriginal == null) return NotFound("Posologia nao encontrada com o Id indicado");

            // Vamos obter a apresentacao indicada pelo IdApresentacao do DTO de entrada.
            var queryA = from a in _context.Apresentacao where a.Id == posologiaDTO.IdApresentacao select a;
            Apresentacao novaApresentacao = await queryA.SingleOrDefaultAsync();

            // Se nao existir, damos erro
            if (novaApresentacao == null) return NotFound("Apresentacao de id indicado nao encontrada com o Id indicado");

            // Construir uma nova posologia para verificar se esta obdece as nossas regras de ser unica nos Servicos.Unico
            Posologia posologiaVerificarUnica = new Posologia() {
                apresentacao = novaApresentacao,
                dosagem = posologiaDTO.dosagem,
                duracao = posologiaDTO.duracao,
                intervalo = posologiaDTO.intervalo
            };

            if (Unicos.IsUnico(_context, posologiaVerificarUnica) == false) return BadRequest("Duplicado");

            // Ja sabemos que a edicao e unica, portanto vamos editar e depois guardar as alterações.
            posologiaOriginal.apresentacao = novaApresentacao;
            posologiaOriginal.dosagem = posologiaDTO.dosagem;
            posologiaOriginal.duracao = posologiaDTO.duracao;
            posologiaOriginal.intervalo = posologiaDTO.intervalo;

            //{


                _context.Entry(posologiaOriginal).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PosologiaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Posologias
        [HttpPost]
        public async Task<IActionResult> PostPosologia([FromBody] CreatePosologiaDTO createPosologiaDTO)
        {
            if (! createPosologiaDTO.Valid()) return BadRequest(createPosologiaDTO);

            // Obtemos a apresentacao indicado por ID no DTO
            var query = from a in _context.Apresentacao
                        where a.Id == createPosologiaDTO.IdApresentacao
                        select a;
            Apresentacao apresentacao = await query.SingleOrDefaultAsync();

            if (apresentacao == null) return BadRequest(createPosologiaDTO); // Confirmamos que exite posologia com aquele id

            // Criamos posologia.
            Posologia posologia = new Posologia()
            {
                apresentacao = apresentacao,
                dosagem = createPosologiaDTO.dosagem,
                duracao = createPosologiaDTO.duracao,
                intervalo = createPosologiaDTO.intervalo
            };

            if (Unicos.IsUnico(_context, posologia) == false) return BadRequest("Duplicado");
            

            _context.Posologia.Add(posologia);
            await _context.SaveChangesAsync();  // Gravamos

            PosologiaDTO dto = DTOFactory.Build(posologia, createPosologiaDTO.IdApresentacao);

            return CreatedAtAction("GetPosologia", new { id = posologia.Id }, dto);
        }

/* Old POST:
        public async Task<IActionResult> PostPosologia([FromBody] Posologia posologia)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Posologia.Add(posologia);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPosologia", new { id = posologia.Id }, DTOFactory.Build(posologia));
        }
*/



        // DELETE: api/Posologias/5
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> DeletePosologia([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var posologia = await _context.Posologia.SingleOrDefaultAsync(m => m.Id == id);
            if (posologia == null)
            {
                return NotFound();
            }

            _context.Posologia.Remove(posologia);
            await _context.SaveChangesAsync();

            return Ok(DTOFactory.Build(posologia));
        }

        private bool PosologiaExists(long id)
        {
            return _context.Posologia.Any(e => e.Id == id);
        }
    }
}