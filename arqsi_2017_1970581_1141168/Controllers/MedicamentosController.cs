﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using arqsi_2017_1970581_1141168.Models;
using arqsi_2017_1970581_1141168.DTO;
using Microsoft.AspNetCore.Authorization;
using arqsi_2017_1970581_1141168.Servicos;

namespace arqsi_2017_1970581_1141168.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Medicamentos")]
    public class MedicamentosController : Controller
    {
        private readonly arqsi_2017_1970581_1141168Context _context;

        public MedicamentosController(arqsi_2017_1970581_1141168Context context)
        {
            _context = context;
        }

        // GET: api/Medicamentos
        [HttpGet]
        public IEnumerable<MedicamentoDTO> GetMedicamento(string nome)
        {
            if (nome == null) return DTOFactory.BuildList(_context.Medicamento);  // Se nao existir ?nome="..." retorna todos os Medicamentos.

            //A existir retorna os com o ?nome=X
            var query = from m in _context.Medicamento where m.nome == nome select m;
            return DTOFactory.BuildList(query);            
        }

        // GET: api/Medicamentos/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMedicamento([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medicamento = await _context.Medicamento.SingleOrDefaultAsync(m => m.Id == id);

            if (medicamento == null)
            {
                return NotFound();
            }

            return Ok(DTOFactory.Build(medicamento));
        }

        // PUT: api/Medicamentos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMedicamento([FromRoute] long id, [FromBody] Medicamento medicamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != medicamento.Id)
            {
                return BadRequest();
            }

            if (Unicos.IsUnico(_context, medicamento) == false)
            {
                return BadRequest("Duplicado");
            }

            _context.Entry(medicamento).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedicamentoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Medicamentos
        [HttpPost]
        public async Task<IActionResult> PostMedicamento([FromBody] Medicamento medicamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (Unicos.IsUnico(_context, medicamento) == false)
            {
                return BadRequest("Duplicado");
            }

            _context.Medicamento.Add(medicamento);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMedicamento", new { id = medicamento.Id }, DTOFactory.Build(medicamento));
        }

        // DELETE: api/Medicamentos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMedicamento([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medicamento = await _context.Medicamento.SingleOrDefaultAsync(m => m.Id == id);
            if (medicamento == null)
            {
                return NotFound();
            }

            _context.Medicamento.Remove(medicamento);
            await _context.SaveChangesAsync();

            return Ok(DTOFactory.Build(medicamento));
        }

        private bool MedicamentoExists(long id)
        {
            return _context.Medicamento.Any(e => e.Id == id);
        }


        // GET: api/Medicamentos/5/Apresentacoes - mostra todas as apresentacoes de um Medicamento de Id.
        [HttpGet("{id:int}/Apresentacoes")]
        public async Task<IActionResult> GetApresentacoesDeMedicamento([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Verificamos se exite medicamento de ID X
            var medicamento = await _context.Medicamento.SingleOrDefaultAsync(m => m.Id == id);

            if (medicamento == null)
            {
                return NotFound();
            }

            // SQL query:  retorna a lista de apresentacoes (Funciona)
            //var lista = from a in _context.Apresentacao where a.farmaco.Id == farmaco.Id select a;
            //return Ok(DTOFactory.BuildList(lista));
            
            // SQL query: retorna a lista de Id de apresentacoes.
            //var query = from ap in _context.MedicamentoApresentacao where ap.medicamento.Id == medicamento.Id select ap.apresentacao.Id;
            
            var medicamentoApresentacao = _context.MedicamentoApresentacao.Where(m => m.medicamento.Id == id).Include(i => i.apresentacao.farmaco);
            
            List<ApresentacaoDTO> resposta = new List<ApresentacaoDTO>();
            foreach (MedicamentoApresentacao ma in medicamentoApresentacao)
            {   
                resposta.Add(DTOFactory.Build(ma.apresentacao, ma.apresentacao.farmaco.Id, ma.apresentacao.farmaco.nome));
            }
            return Ok(resposta);
        }

        // GET: api/Medicamentos/5/Apresentacoes - mostra todas as apresentacoes de um Medicamento de Id.
        [HttpGet("{id:int}/Posologias")]
        public async Task<IActionResult> GetPosologiasDeMedicamento([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Verificamos se exite medicamento de ID X
            var medicamento = await _context.Medicamento.SingleOrDefaultAsync(m => m.Id == id);

            if (medicamento == null)
            {
                return NotFound();
            }

            
            // Query LINK tipo SQL para obter os Ids de possologias das apresentacoes que estao ligadas ao medicamento de Id X atravez de ApresentacaoMedicamento
            
            /*  Antiga, so devolvia os ID's.
            var query = from map in _context.MedicamentoApresentacao join pos in _context.Posologia 
                         on map.apresentacao.Id equals pos.apresentacao.Id
                         where map.medicamento.Id == id
                         select pos.Id;
            */

            var posologias = from map in _context.MedicamentoApresentacao
                            join pos in _context.Posologia.Include(a => a.apresentacao)
                            on map.apresentacao.Id equals pos.apresentacao.Id
                            where map.medicamento.Id == id
                            select pos ;

            List<PosologiaDTO> resposta = new List<PosologiaDTO>();
            foreach (Posologia p in posologias)
            {
                resposta.Add(DTOFactory.Build(p, p.apresentacao.Id));
            }


            return Ok(resposta);
            //return Ok(query);
        }
    }
}