﻿using arqsi_2017_1970581_1141168.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace arqsi_2017_1970581_1141168.Servicos
{
    public static class Unicos
    {
        public static bool IsUnico(arqsi_2017_1970581_1141168Context context , Farmaco farmaco) {

            return (context.Farmaco.FirstOrDefault(f => f.nome == farmaco.nome) == null);
            
        }
        public static bool IsUnico(arqsi_2017_1970581_1141168Context context, Medicamento medicamento)
        {

            return (context.Medicamento.FirstOrDefault(m => m.nome == medicamento.nome) == null);

        }

        public static bool IsUnico(arqsi_2017_1970581_1141168Context context, Apresentacao apresentacao)
        {

            return (context.Apresentacao.FirstOrDefault(a => a.farmaco.nome == apresentacao.farmaco.nome && 
            a.forma == apresentacao.forma &&
            a.qtd == apresentacao.qtd &&
            a.concentracao == a.concentracao) == null);

        }

        public static bool IsUnico(arqsi_2017_1970581_1141168Context context, Posologia posologia)
        {

            return (context.Posologia.FirstOrDefault(p => p.apresentacao.Id == posologia.apresentacao.Id &&
            p.dosagem == posologia.dosagem &&
            p.duracao == posologia.duracao &&
            p.intervalo == posologia.intervalo) == null);

        }

        public static bool IsUnico(arqsi_2017_1970581_1141168Context context, MedicamentoApresentacao medicamentoApresentacao)
        {

            return (context.MedicamentoApresentacao.FirstOrDefault(ma => ma.medicamento.Id == medicamentoApresentacao.medicamento.Id &&
            ma.apresentacao.Id == medicamentoApresentacao.apresentacao.Id ) == null);

        }


    }
}
