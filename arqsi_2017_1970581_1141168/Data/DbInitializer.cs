﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using arqsi_2017_1970581_1141168.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace arqsi_2017_1970581_1141168.Data
{
    public class DbInitializer
    {
        public static void Initialize(arqsi_2017_1970581_1141168Context context)
        {
            context.Database.EnsureCreated();

            // Verifica se ja existem medicamentos.
            if (context.Medicamento.Any())
            {
                return;   // Já foi inicializada
            }

            var medicamentos = new Medicamento[]
            {
            new Medicamento{nome = "Brufen" },
            new Medicamento{nome = "Ben-u-ron"},
            new Medicamento{nome = "Aspegic"},
            new Medicamento{nome = "Cegripe"},
            new Medicamento{nome = "Mejoral"},
            new Medicamento{nome = "Tylenol"},
            // Medicamentos de teste
            new Medicamento{nome = "mf01"},
            new Medicamento{nome = "mf02"},

            };
            foreach (Medicamento m in medicamentos)
            {
                context.Medicamento.Add(m);
            }
            context.SaveChanges();

            var farmacos = new Farmaco[]
            {
            new Farmaco{nome = "Acetaminofeno"},
            new Farmaco{nome = "Ibuprofeno"},
            new Farmaco{nome = "Acetilsalicilato de lisina"},
            new Farmaco{nome = "Ácido Acetil-Salicílico"},
            new Farmaco{nome = "Diclofenaco Sódico"},
            // Farmacos de teste.
            new Farmaco{nome = "f01"},
            new Farmaco{nome = "f02"},

            };
            foreach (Farmaco f in farmacos)
            {
                context.Farmaco.Add(f);
            }
            context.SaveChanges();

            
            // Criar lista de apresentacoes para cada farmaco.
            IEnumerable<Farmaco> listaFarmaco = new List<Farmaco>();
            listaFarmaco = context.Farmaco;

            foreach (Farmaco f in listaFarmaco) {
                Apresentacao ap = new Apresentacao { farmaco = f, concentracao = "10", forma = "comprimidos", qtd = "24" };
                Apresentacao ap2 = new Apresentacao { farmaco = f, concentracao = "15", forma = "xarope", qtd = "75" };
                context.Apresentacao.Add(ap);
                context.Apresentacao.Add(ap2);
            }
            context.SaveChanges();

            // Cria 2 possologias para cada apresentacao.
            foreach (Apresentacao ap in context.Apresentacao) {
                Posologia posologia = new Posologia { apresentacao = ap, dosagem = 2 ,intervalo = 8, duracao = 8 };
                Posologia posologia2 = new Posologia { apresentacao = ap, dosagem = 5, intervalo = 12, duracao = 8 };
                context.Posologia.Add(posologia);
                context.Posologia.Add(posologia2);
            }
            context.SaveChanges();

            //ApresentacaoMedicamento de teste:
            // farmaco f01
            var query = from ap in context.Apresentacao where ap.farmaco.nome == "f01" select ap;
            var querym = from m in context.Medicamento where m.nome == "mf01" select m;
            Medicamento med01 = querym.First(); 
            foreach (Apresentacao ap in query){
                MedicamentoApresentacao ma = new MedicamentoApresentacao { apresentacao = ap, medicamento = med01 };
                context.MedicamentoApresentacao.Add(ma);
            }
            context.SaveChanges();
            // farmaco f02
            var query2 = from ap in context.Apresentacao where ap.farmaco.nome == "f02" select ap;
            var querym2 = from m in context.Medicamento where m.nome == "mf02" select m;
            Medicamento med02 = querym2.First();
            foreach (Apresentacao ap in query2)
            {
                MedicamentoApresentacao ma = new MedicamentoApresentacao { apresentacao = ap, medicamento = med02 };
                context.MedicamentoApresentacao.Add(ma);
            }
            context.SaveChanges();


            // Novo utilizador.

            // NAO FUNCIONA!!!

            var user = new UserEntity {
                UserName = "gestaoreceita@isep.ipp.pt",
                Email = "gestaoreceita@isep.ipp.pt",
                NormalizedEmail = "GESTAORECEITA@ISEP.IPP.PT",
                NormalizedUserName = "GESTAORECEITA@ISEP.IPP.PT",
                LockoutEnabled = true,
                SecurityStamp = Guid.NewGuid().ToString()
            };


            IPasswordHasher<UserEntity> passwordHasher = new PasswordHasher<UserEntity>();            
            string password = passwordHasher.HashPassword(user, "Qwerty1!");
            user.PasswordHash = password;
            context.UserEntity.Add(user);
            context.SaveChanges();

            int i = 0;
            i++;

        }
}
}
