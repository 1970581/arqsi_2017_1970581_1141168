﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using arqsi_2017_1970581_1141168.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace arqsi_2017_1970581_1141168.Models
{
    public class arqsi_2017_1970581_1141168Context : IdentityDbContext<UserEntity>
    {
        public arqsi_2017_1970581_1141168Context (DbContextOptions<arqsi_2017_1970581_1141168Context> options)
            : base(options)
        {
        }

        public DbSet<arqsi_2017_1970581_1141168.Models.Medicamento> Medicamento { get; set; }

        public DbSet<arqsi_2017_1970581_1141168.Models.Farmaco> Farmaco { get; set; }

        public DbSet<arqsi_2017_1970581_1141168.Models.Apresentacao> Apresentacao { get; set; }

        public DbSet<arqsi_2017_1970581_1141168.Models.MedicamentoApresentacao> MedicamentoApresentacao { get; set; }

        public DbSet<arqsi_2017_1970581_1141168.Models.Posologia> Posologia { get; set; }

        public DbSet<arqsi_2017_1970581_1141168.Models.UserEntity> UserEntity { get; set; }
    }
}
