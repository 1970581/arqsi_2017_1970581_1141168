﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace arqsi_2017_1970581_1141168.DTO
{
    public class CreatePosologiaDTO
    {        
        public long IdApresentacao { get; set; }
        public int duracao { get; set; }//dias
        public int intervalo { get; set; } //horas
        public int dosagem { get; set; }  // quantidade

        public bool Valid() {
            return IdApresentacao > 0 && dosagem > 0;
        }
    }
}
