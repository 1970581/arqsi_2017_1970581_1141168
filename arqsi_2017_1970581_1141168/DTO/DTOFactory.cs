﻿using arqsi_2017_1970581_1141168.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace arqsi_2017_1970581_1141168.DTO
{   /// <summary>
    /// Classe responsavel por criar os DTOs de cada um dos objectos do model
    /// </summary>
    public static class DTOFactory
    {
        /// <summary>
        /// Recebe um farmaco e retorna sob a forma de FarmacoDTO
        /// </summary>
        /// <param name="farmaco"></param>
        /// <returns></returns>
        public static FarmacoDTO Build(Farmaco farmaco) {

            //Cria um objecto FarmacoDTO e retorna-o.
            return new FarmacoDTO() { Id = farmaco.Id, nome = farmaco.nome     };
            
        }

        /// <summary>
        /// Recebe uma collection de Farmaco e devolve outra collection de FarmacoDTO
        /// </summary>
        /// <param name="listaFarmaco"></param>
        /// <returns></returns>
        public static IEnumerable<FarmacoDTO> BuildList(IEnumerable<Farmaco> listaFarmaco) {
            List<FarmacoDTO> resposta = new List<FarmacoDTO>();

            foreach (Farmaco f in listaFarmaco) {
                resposta.Add(Build(f));
            }
            return resposta;
        }
                      

        public static MedicamentoDTO Build(Medicamento medicamento) {
            return new MedicamentoDTO() { Id = medicamento.Id, nome = medicamento.nome };
        }

        public static IEnumerable<MedicamentoDTO> BuildList(IEnumerable<Medicamento> listaMedicamento) {
            List<MedicamentoDTO> resposta = new List<MedicamentoDTO>();

            foreach (Medicamento obj in listaMedicamento)
            {
                resposta.Add(Build(obj));
            }
            return resposta;
        }


        public static PosologiaDTO Build(Posologia posologia) {  
            

            return new PosologiaDTO()
            {
                Id = posologia.Id,
                dosagem = posologia.dosagem,
                duracao = posologia.duracao,
                intervalo = posologia.intervalo
            };
        }

        public static PosologiaDTO Build(Posologia posologia, long IdApresentacao)
        {
            return new PosologiaDTO()
            {
                Id = posologia.Id,
                IdApresentacao = IdApresentacao,
                dosagem = posologia.dosagem,
                duracao = posologia.duracao,
                intervalo = posologia.intervalo
            };
        }

        public static IEnumerable<PosologiaDTO> BuildList(IEnumerable<Posologia> listaPosologia)
        {
            List<PosologiaDTO> resposta = new List<PosologiaDTO>();

            foreach (Posologia obj in listaPosologia)
            {
                resposta.Add(Build(obj));
            }
            return resposta;
        }



        public static ApresentacaoDTO Build(Apresentacao apresentacao) {
            return new ApresentacaoDTO() {
                Id = apresentacao.Id,
                forma = apresentacao.forma,
                concentracao = apresentacao.concentracao.ToString() + "mg" , // coloca mg no final
                qtd = apresentacao.qtd
            };
        }

        public static ApresentacaoDTO Build(Apresentacao apresentacao, long IdFarmaco)
        {
            return new ApresentacaoDTO()
            {
                Id = apresentacao.Id,                
                IdFarmaco = IdFarmaco,
                forma = apresentacao.forma,
                concentracao = apresentacao.concentracao.ToString() + "mg", // coloca mg no final
                qtd = apresentacao.qtd
            };
        }

        public static ApresentacaoDTO Build(Apresentacao apresentacao, long IdFarmaco, String nomefarmaco)
        {
            return new ApresentacaoDTO()
            {
                Id = apresentacao.Id,
                IdFarmaco = IdFarmaco,
                nomeFarmaco = nomefarmaco,
                forma = apresentacao.forma,
                concentracao = apresentacao.concentracao.ToString() + "mg", // coloca mg no final
                qtd = apresentacao.qtd
            };
        }

        public static IEnumerable<ApresentacaoDTO> BuildList(IEnumerable<Apresentacao> listaApresentacao)
        {
            List<ApresentacaoDTO> resposta = new List<ApresentacaoDTO>();

            foreach (Apresentacao obj in listaApresentacao)
            {
                resposta.Add(Build(obj));
            }
            return resposta;
        }


    }
}
