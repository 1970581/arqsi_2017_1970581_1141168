﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace arqsi_2017_1970581_1141168.DTO
{
    public class ApresentacaoDTO
    {
        public long Id { get; set; }
        public long IdFarmaco { get; set; }
        public String nomeFarmaco { get; set; }
        public String forma { get; set; }        
        public String concentracao { get; set; }
        public String qtd { get; set; }
    }
}
