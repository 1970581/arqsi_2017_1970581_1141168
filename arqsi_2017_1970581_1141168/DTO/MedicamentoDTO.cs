﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace arqsi_2017_1970581_1141168.DTO
{
    public class MedicamentoDTO
    {
        public long Id { get; set; }
        public String nome { get; set; }
    }
}
