﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace arqsi_2017_1970581_1141168.DTO
{
    public class CreateApresentacaoDTO
    {
        public long IdFarmaco { get; set; }
        public String forma { get; set; }
        public String qtd { get; set; }
        public String concentracao { get; set; }

        public bool Valid()
        {
            return IdFarmaco > 0 && !String.IsNullOrEmpty(forma) &&
                !String.IsNullOrEmpty(qtd) &&
                !String.IsNullOrEmpty(concentracao);
        }
    }
}
