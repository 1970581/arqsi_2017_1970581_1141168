﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace arqsi_2017_1970581_1141168.Models
{
    public class MedicamentoApresentacao
    {
        public long Id { get; set; }

        [Required]
        public Medicamento medicamento { get; set; }

        [Required]
        public Apresentacao apresentacao { get; set; }
    }
}
