﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace arqsi_2017_1970581_1141168.Models
{
    public class Apresentacao
    {
        public long Id { get; set; }

        [Required]
        public Farmaco farmaco { get; set; } 

        //public MedicamentoApresentacao medicamentoApresentacao { get; set; }
        [Required]
        public String forma { get; set; }
        [Required]
        public String qtd { get; set; }
        [Required]
        public String concentracao { get; set; } 

    }
}
