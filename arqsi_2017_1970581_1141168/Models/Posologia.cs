﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace arqsi_2017_1970581_1141168.Models
{
    public class Posologia
    {
        public long Id { get; set; }

        [Required]
        public Apresentacao apresentacao { get; set; }

        [Required]
        public int duracao { get; set; }//dias

        [Required]
        public int intervalo { get; set; } //horas

        [Required]
        public int dosagem { get; set; } 



    }
}
