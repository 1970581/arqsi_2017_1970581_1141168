﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace arqsi_2017_1970581_1141168.Migrations
{
    public partial class m2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "idApresentacao",
                table: "MedicamentoApresentacao");

            migrationBuilder.DropColumn(
                name: "idMedicamento",
                table: "MedicamentoApresentacao");

            migrationBuilder.RenameColumn(
                name: "Marca",
                table: "Medicamento",
                newName: "marca");

            migrationBuilder.RenameColumn(
                name: "PrincipioAtivo",
                table: "Farmaco",
                newName: "principioAtivo");

            migrationBuilder.RenameColumn(
                name: "Quantidade",
                table: "Apresentacao",
                newName: "quantidade");

            migrationBuilder.RenameColumn(
                name: "Forma",
                table: "Apresentacao",
                newName: "forma");

            migrationBuilder.AddColumn<long>(
                name: "apresentacaoId",
                table: "MedicamentoApresentacao",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "medicamentoId",
                table: "MedicamentoApresentacao",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "farmacoId",
                table: "Apresentacao",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_MedicamentoApresentacao_apresentacaoId",
                table: "MedicamentoApresentacao",
                column: "apresentacaoId");

            migrationBuilder.CreateIndex(
                name: "IX_MedicamentoApresentacao_medicamentoId",
                table: "MedicamentoApresentacao",
                column: "medicamentoId");

            migrationBuilder.CreateIndex(
                name: "IX_Apresentacao_farmacoId",
                table: "Apresentacao",
                column: "farmacoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Apresentacao_Farmaco_farmacoId",
                table: "Apresentacao",
                column: "farmacoId",
                principalTable: "Farmaco",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MedicamentoApresentacao_Apresentacao_apresentacaoId",
                table: "MedicamentoApresentacao",
                column: "apresentacaoId",
                principalTable: "Apresentacao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MedicamentoApresentacao_Medicamento_medicamentoId",
                table: "MedicamentoApresentacao",
                column: "medicamentoId",
                principalTable: "Medicamento",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Apresentacao_Farmaco_farmacoId",
                table: "Apresentacao");

            migrationBuilder.DropForeignKey(
                name: "FK_MedicamentoApresentacao_Apresentacao_apresentacaoId",
                table: "MedicamentoApresentacao");

            migrationBuilder.DropForeignKey(
                name: "FK_MedicamentoApresentacao_Medicamento_medicamentoId",
                table: "MedicamentoApresentacao");

            migrationBuilder.DropIndex(
                name: "IX_MedicamentoApresentacao_apresentacaoId",
                table: "MedicamentoApresentacao");

            migrationBuilder.DropIndex(
                name: "IX_MedicamentoApresentacao_medicamentoId",
                table: "MedicamentoApresentacao");

            migrationBuilder.DropIndex(
                name: "IX_Apresentacao_farmacoId",
                table: "Apresentacao");

            migrationBuilder.DropColumn(
                name: "apresentacaoId",
                table: "MedicamentoApresentacao");

            migrationBuilder.DropColumn(
                name: "medicamentoId",
                table: "MedicamentoApresentacao");

            migrationBuilder.DropColumn(
                name: "farmacoId",
                table: "Apresentacao");

            migrationBuilder.RenameColumn(
                name: "marca",
                table: "Medicamento",
                newName: "Marca");

            migrationBuilder.RenameColumn(
                name: "principioAtivo",
                table: "Farmaco",
                newName: "PrincipioAtivo");

            migrationBuilder.RenameColumn(
                name: "quantidade",
                table: "Apresentacao",
                newName: "Quantidade");

            migrationBuilder.RenameColumn(
                name: "forma",
                table: "Apresentacao",
                newName: "Forma");

            migrationBuilder.AddColumn<long>(
                name: "idApresentacao",
                table: "MedicamentoApresentacao",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "idMedicamento",
                table: "MedicamentoApresentacao",
                nullable: false,
                defaultValue: 0L);
        }
    }
}
