﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace arqsi_2017_1970581_1141168.Migrations
{
    public partial class m3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "apresentacaoId",
                table: "Posologia",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "dosagem",
                table: "Posologia",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "duracao",
                table: "Posologia",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "intervalo",
                table: "Posologia",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Posologia_apresentacaoId",
                table: "Posologia",
                column: "apresentacaoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Posologia_Apresentacao_apresentacaoId",
                table: "Posologia",
                column: "apresentacaoId",
                principalTable: "Apresentacao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Posologia_Apresentacao_apresentacaoId",
                table: "Posologia");

            migrationBuilder.DropIndex(
                name: "IX_Posologia_apresentacaoId",
                table: "Posologia");

            migrationBuilder.DropColumn(
                name: "apresentacaoId",
                table: "Posologia");

            migrationBuilder.DropColumn(
                name: "dosagem",
                table: "Posologia");

            migrationBuilder.DropColumn(
                name: "duracao",
                table: "Posologia");

            migrationBuilder.DropColumn(
                name: "intervalo",
                table: "Posologia");
        }
    }
}
