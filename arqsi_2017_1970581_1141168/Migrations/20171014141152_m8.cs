﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace arqsi_2017_1970581_1141168.Migrations
{
    public partial class m8 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Apresentacao_Farmaco_farmacoId",
                table: "Apresentacao");

            migrationBuilder.DropForeignKey(
                name: "FK_MedicamentoApresentacao_Apresentacao_apresentacaoId",
                table: "MedicamentoApresentacao");

            migrationBuilder.DropForeignKey(
                name: "FK_MedicamentoApresentacao_Medicamento_medicamentoId",
                table: "MedicamentoApresentacao");

            migrationBuilder.DropForeignKey(
                name: "FK_Posologia_Apresentacao_apresentacaoId",
                table: "Posologia");

            migrationBuilder.AlterColumn<long>(
                name: "apresentacaoId",
                table: "Posologia",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "medicamentoId",
                table: "MedicamentoApresentacao",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "apresentacaoId",
                table: "MedicamentoApresentacao",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "nome",
                table: "Medicamento",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "nome",
                table: "Farmaco",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "qtd",
                table: "Apresentacao",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "forma",
                table: "Apresentacao",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "farmacoId",
                table: "Apresentacao",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "concentracao",
                table: "Apresentacao",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Apresentacao_Farmaco_farmacoId",
                table: "Apresentacao",
                column: "farmacoId",
                principalTable: "Farmaco",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MedicamentoApresentacao_Apresentacao_apresentacaoId",
                table: "MedicamentoApresentacao",
                column: "apresentacaoId",
                principalTable: "Apresentacao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MedicamentoApresentacao_Medicamento_medicamentoId",
                table: "MedicamentoApresentacao",
                column: "medicamentoId",
                principalTable: "Medicamento",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Posologia_Apresentacao_apresentacaoId",
                table: "Posologia",
                column: "apresentacaoId",
                principalTable: "Apresentacao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Apresentacao_Farmaco_farmacoId",
                table: "Apresentacao");

            migrationBuilder.DropForeignKey(
                name: "FK_MedicamentoApresentacao_Apresentacao_apresentacaoId",
                table: "MedicamentoApresentacao");

            migrationBuilder.DropForeignKey(
                name: "FK_MedicamentoApresentacao_Medicamento_medicamentoId",
                table: "MedicamentoApresentacao");

            migrationBuilder.DropForeignKey(
                name: "FK_Posologia_Apresentacao_apresentacaoId",
                table: "Posologia");

            migrationBuilder.AlterColumn<long>(
                name: "apresentacaoId",
                table: "Posologia",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<long>(
                name: "medicamentoId",
                table: "MedicamentoApresentacao",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<long>(
                name: "apresentacaoId",
                table: "MedicamentoApresentacao",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<string>(
                name: "nome",
                table: "Medicamento",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "nome",
                table: "Farmaco",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<int>(
                name: "qtd",
                table: "Apresentacao",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "forma",
                table: "Apresentacao",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<long>(
                name: "farmacoId",
                table: "Apresentacao",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<int>(
                name: "concentracao",
                table: "Apresentacao",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AddForeignKey(
                name: "FK_Apresentacao_Farmaco_farmacoId",
                table: "Apresentacao",
                column: "farmacoId",
                principalTable: "Farmaco",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MedicamentoApresentacao_Apresentacao_apresentacaoId",
                table: "MedicamentoApresentacao",
                column: "apresentacaoId",
                principalTable: "Apresentacao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MedicamentoApresentacao_Medicamento_medicamentoId",
                table: "MedicamentoApresentacao",
                column: "medicamentoId",
                principalTable: "Medicamento",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Posologia_Apresentacao_apresentacaoId",
                table: "Posologia",
                column: "apresentacaoId",
                principalTable: "Apresentacao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
