﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace arqsi_2017_1970581_1141168.Migrations
{
    public partial class m4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "marca",
                table: "Medicamento");

            migrationBuilder.DropColumn(
                name: "principioAtivo",
                table: "Farmaco");

            migrationBuilder.DropColumn(
                name: "dosagem",
                table: "Apresentacao");

            migrationBuilder.DropColumn(
                name: "quantidade",
                table: "Apresentacao");

            migrationBuilder.AddColumn<string>(
                name: "nome",
                table: "Medicamento",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "nome",
                table: "Farmaco",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "concentracao",
                table: "Apresentacao",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "qtd",
                table: "Apresentacao",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "nome",
                table: "Medicamento");

            migrationBuilder.DropColumn(
                name: "nome",
                table: "Farmaco");

            migrationBuilder.DropColumn(
                name: "concentracao",
                table: "Apresentacao");

            migrationBuilder.DropColumn(
                name: "qtd",
                table: "Apresentacao");

            migrationBuilder.AddColumn<string>(
                name: "marca",
                table: "Medicamento",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "principioAtivo",
                table: "Farmaco",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "dosagem",
                table: "Apresentacao",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "quantidade",
                table: "Apresentacao",
                nullable: false,
                defaultValue: 0);
        }
    }
}
